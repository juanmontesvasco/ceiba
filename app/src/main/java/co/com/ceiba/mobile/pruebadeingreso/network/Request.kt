package co.com.ceiba.mobile.pruebadeingreso.network

import android.content.Context
import com.android.volley.*
import com.android.volley.Response.Listener
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import java.util.HashMap


class Request(val context: Context) {
    var queue: RequestQueue = Volley.newRequestQueue(context)

    fun getError(volleyError: VolleyError): String {
        if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
            val error = VolleyError(String(volleyError.networkResponse.data))
            return error.message.toString()
        }
        return ""
    }

    fun getHttp(url: String?,
                headers: Map<String, String>?,
                response: Listener<String>,
                error: Response.ErrorListener?): StringRequest? {
        val postRequest: StringRequest = object : StringRequest(Method.GET, url,
                response,
                error
        ) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String>? {
                return headers
            }
        }
        postRequest.retryPolicy = DefaultRetryPolicy(
                30000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        queue.add(postRequest)
        return postRequest
    }

    fun getHeaders(): Map<String, String>? {
        val headers: MutableMap<String, String> = HashMap()
        headers["Accept"] = "application/json"
        headers["Content-Type"] = "application/x-www-form-urlencoded"
        return headers
    }
}