package co.com.ceiba.mobile.pruebadeingreso.objects

import org.json.JSONObject

class Geo(geo: JSONObject) {
    var lat: String = geo.getString("lat")
    var lng: String = geo.getString("lng")
}