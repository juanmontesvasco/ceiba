package co.com.ceiba.mobile.pruebadeingreso.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import co.com.ceiba.mobile.pruebadeingreso.R
import co.com.ceiba.mobile.pruebadeingreso.objects.Post

class PostAdapter : RecyclerView.Adapter<PostAdapter.ViewHolder>() {

    var posts: MutableList<Post> = ArrayList()
    private lateinit var context: Context

    fun PostAdapter(posts: MutableList<Post>, context: Context) {
        this.posts = posts
        this.context = context
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = posts[position]
        holder.bind(item)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.post_list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return posts.size
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val title = view.findViewById(R.id.title) as TextView
        private val body = view.findViewById(R.id.body) as TextView


        fun bind(post: Post) {
            title.text = "${post.title}"
            body.text = "${post.body}"
        }
    }
}