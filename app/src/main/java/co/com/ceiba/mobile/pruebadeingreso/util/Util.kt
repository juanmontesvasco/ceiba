package co.com.ceiba.mobile.pruebadeingreso.util

import android.content.Context
import android.widget.Toast
import co.com.ceiba.mobile.pruebadeingreso.R
import java.io.BufferedReader
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.InputStreamReader

class Util(private val context: Context) {

    fun save(fileName: String, fileData: String) {
        val file: String = fileName
        val data: String = fileData
        val fileOutputStream: FileOutputStream
        try {
            fileOutputStream = context.openFileOutput(file, Context.MODE_PRIVATE)
            fileOutputStream.write(data.toByteArray())
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun get(name: String): String {
        if (name != null && name.trim() != "") {
            var fileInputStream: FileInputStream? = null
            fileInputStream = context.openFileInput(name)
            var inputStreamReader: InputStreamReader = InputStreamReader(fileInputStream)
            val bufferedReader: BufferedReader = BufferedReader(inputStreamReader)
            val stringBuilder: StringBuilder = StringBuilder()
            var text: String? = null
            while ({ text = bufferedReader.readLine(); text }() != null) {
                stringBuilder.append(text)
            }
            return stringBuilder.toString()
        }
        return ""
    }

    public fun error() {
        Toast.makeText(context, context.getString(R.string.error), Toast.LENGTH_LONG).show()
    }

    public fun toast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }
}