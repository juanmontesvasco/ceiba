package co.com.ceiba.mobile.pruebadeingreso.view

import android.Manifest
import android.app.ProgressDialog
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.com.ceiba.mobile.pruebadeingreso.R
import co.com.ceiba.mobile.pruebadeingreso.adapters.UsersAdapter
import co.com.ceiba.mobile.pruebadeingreso.network.Request
import co.com.ceiba.mobile.pruebadeingreso.objects.User
import co.com.ceiba.mobile.pruebadeingreso.rest.Endpoints
import co.com.ceiba.mobile.pruebadeingreso.util.Parameters
import co.com.ceiba.mobile.pruebadeingreso.util.Util
import com.android.volley.Response
import com.google.android.material.textfield.TextInputEditText
import org.json.JSONArray
import org.json.JSONException

class MainActivity : AppCompatActivity() {

    private lateinit var context: Context
    private lateinit var request: Request
    private lateinit var pd: ProgressDialog
    private lateinit var listUsers: MutableList<User>
    private lateinit var mRecyclerView: RecyclerView
    private lateinit var search: TextInputEditText
    private val mAdapter: UsersAdapter = UsersAdapter()
    private lateinit var util: Util
    private val PERMS = arrayOf(
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    private val REQUEST = 823

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        getViews()
        addListeners()
        verifyPermissions()
        getUsersFromStorage()
    }

    private fun init() {
        context = this
        util = Util(context)
        request = Request(context)
    }

    private fun getViews() {
        search = findViewById(R.id.editTextSearch)
    }

    private fun addListeners() {
        search.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
                mAdapter.getFilter()?.filter(s)
            }
        })
    }

    private fun setUpRecyclerView() {
        mRecyclerView = findViewById(R.id.recyclerViewSearchResults)
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.layoutManager = LinearLayoutManager(this)
        mAdapter.UsersAdapter(listUsers, this)
        mRecyclerView.adapter = mAdapter
    }

    private fun getHistory() {
        initPd()
        val success = Response.Listener<String> { response ->
            dismissPd()
            showArrayUsers(response)
        }

        val error = Response.ErrorListener { error ->
            dismissPd()
            util.error()
        }

        request.getHttp(
            Endpoints.URL_BASE + Endpoints.GET_USERS,
            request.getHeaders(),
            success,
            error
        )
    }

    fun initPd() {
        pd = ProgressDialog.show(
            context,
            resources.getString(R.string.title_pd),
            resources.getString(R.string.wait),
            true,
            false
        )
    }

    fun dismissPd() {
        try {
            pd.dismiss()
        } catch (e: Exception) {
        }
    }

    private fun showUsers() {
        setUpRecyclerView()
    }

    private fun getUsersFromStorage() {
        try {
            if (util.get(Parameters.USERS).isEmpty()) {
                getHistory()
            } else {
                showArrayUsers(util.get(Parameters.USERS))
            }
        } catch (e: java.lang.Exception) {
            getHistory()
        }
    }

    private fun showArrayUsers(response: String) {
        try {
            val json = JSONArray(response)
            util.save(Parameters.USERS, response)
            if (json.length() > 0) {
                listUsers = ArrayList()
                for (i in 0 until json.length()) {
                    var user = User(json.getJSONObject(i))
                    listUsers.add(user)
                }
                showUsers()
            } else {
                util.toast(getString(R.string.no_users))
            }

        } catch (e: JSONException) {
            util.error()
        }
    }

    private fun hasPermission(perm: String): Boolean {
        return try {
            PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this, perm)
        } catch (e: java.lang.Exception) {
            true
        }
    }

    private fun canAccessWrite(): Boolean {
        return hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST) {
            if (!canAccessWrite()) {
                util.toast(getString(R.string.permissions))
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun verifyPermissions() {
        if (!canAccessWrite()) {
            requestPermissions(
                PERMS,
                REQUEST
            )
        }
    }
}