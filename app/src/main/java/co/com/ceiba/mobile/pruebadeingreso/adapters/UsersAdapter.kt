package co.com.ceiba.mobile.pruebadeingreso.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Filter
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import co.com.ceiba.mobile.pruebadeingreso.R
import co.com.ceiba.mobile.pruebadeingreso.objects.User
import co.com.ceiba.mobile.pruebadeingreso.view.PostActivity

class UsersAdapter : RecyclerView.Adapter<UsersAdapter.ViewHolder>() {

    var users: MutableList<User> = ArrayList()
    var filter: MutableList<User> = ArrayList()
    private lateinit var context: Context
    private var previousValue = -1

    fun UsersAdapter(users: MutableList<User>, context: Context) {
        this.users = users
        this.context = context
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = users[position]
        holder.bind(item, context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.user_list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return users.size
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name = view.findViewById(R.id.name) as TextView
        val phone = view.findViewById(R.id.phone) as TextView
        val email = view.findViewById(R.id.email) as TextView
        val viewPost = view.findViewById(R.id.btn_view_post) as Button


        fun bind(user: User, context: Context) {
            name.text = "${user.name}"
            phone.text = "${user.phone}"
            email.text = "${user.email}"

            viewPost.setOnClickListener {
                val intent = Intent(context, PostActivity::class.java).apply {
                    putExtra("user_id", user.id)
                    putExtra("name", user.name)
                    putExtra("phone", user.phone)
                    putExtra("email", user.email)
                }
                context.startActivity(intent)
            }
        }
    }

    fun getFilter(): Filter? {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence): FilterResults {
                var oReturn = FilterResults()
                var results: MutableList<User> = ArrayList()
                if (filter == null || filter.isEmpty()) {
                    filter = users
                }
                if (constraint != null) {
                    if (filter.size > 0) {
                        for (u in filter) {
                            if (u.name.toLowerCase().contains(constraint.toString())) {
                                results.add(u)
                            }
                        }
                    }
                    oReturn.values = results
                }
                return oReturn
            }

            override fun publishResults(constraint: CharSequence, results: FilterResults) {
                users = results.values as MutableList<User>
                if (users.size == 0 && previousValue != 0) {
                    Toast.makeText(
                        context,
                        context.getString(R.string.empty_list),
                        Toast.LENGTH_LONG
                    ).show()
                }
                previousValue = users.size
                notifyDataSetChanged()
            }
        }
    }
}