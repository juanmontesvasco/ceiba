package co.com.ceiba.mobile.pruebadeingreso.view

import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.com.ceiba.mobile.pruebadeingreso.R
import co.com.ceiba.mobile.pruebadeingreso.adapters.PostAdapter
import co.com.ceiba.mobile.pruebadeingreso.network.Request
import co.com.ceiba.mobile.pruebadeingreso.objects.Post
import co.com.ceiba.mobile.pruebadeingreso.rest.Endpoints
import co.com.ceiba.mobile.pruebadeingreso.util.Util
import com.android.volley.Response
import org.json.JSONArray
import org.json.JSONException

class PostActivity : AppCompatActivity() {

    private lateinit var context: Context
    private lateinit var request: Request
    private lateinit var pd: ProgressDialog
    private lateinit var userId: String
    private lateinit var listPost: MutableList<Post>
    private lateinit var mRecyclerView: RecyclerView
    private val mAdapter: PostAdapter = PostAdapter()
    private lateinit var util: Util

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post)
        init()
        getData()
        getPost()
    }

    private fun init() {
        context = this
        util = Util(context)
        request = Request(context)
    }


    private fun getData() {
        val name: TextView = findViewById(R.id.name)
        val phone: TextView = findViewById(R.id.phone)
        val email: TextView = findViewById(R.id.email)
        userId = intent.getStringExtra("user_id").toString()
        name.text = intent.getStringExtra("name").toString()
        phone.text = intent.getStringExtra("phone").toString()
        email.text = intent.getStringExtra("email").toString()
    }

    fun setUpRecyclerView() {
        mRecyclerView = findViewById(R.id.recyclerViewPostsResults)
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.layoutManager = LinearLayoutManager(this)
        mAdapter.PostAdapter(listPost, this)
        mRecyclerView.adapter = mAdapter
    }


    private fun getPost() {
        initPd()
        val success = Response.Listener<String> { response ->
            dismissPd()
            showArrayPost(response)
        }

        val error = Response.ErrorListener { error ->
            dismissPd()
            util.error()
        }

        request.getHttp(
            Endpoints.URL_BASE + Endpoints.GET_POST_USER + userId,
            request.getHeaders(),
            success,
            error
        )
    }

    fun initPd() {
        pd = ProgressDialog.show(
            context,
            resources.getString(R.string.title_pd),
            resources.getString(R.string.wait),
            true,
            false
        )
    }

    fun dismissPd() {
        try {
            pd.dismiss()
        } catch (e: Exception) {
        }
    }

    private fun showArrayPost(response: String) {
        try {
            val json = JSONArray(response)
            if (json.length() > 0) {
                listPost = ArrayList()
                for (i in 0 until json.length()) {
                    var post = Post(json.getJSONObject(i))
                    listPost.add(post)
                }
                showPost()
            } else {
                util.toast(getString(R.string.no_post))
            }

        } catch (e: JSONException) {
            util.error()
        }
    }

    private fun showPost() {
        setUpRecyclerView()
    }
}