package co.com.ceiba.mobile.pruebadeingreso.objects

import org.json.JSONObject

class User(user: JSONObject){
    var id: String = user.getString("id")
    var name: String = user.getString("name")
    var username: String = user.getString("username")
    var email: String= user.getString("email")
    var address: Address= Address(user.getJSONObject("address"))
    var phone: String= user.getString("phone")
    var website: String= user.getString("website")
    var company: Company= Company(user.getJSONObject("company"))
}