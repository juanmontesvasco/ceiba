package co.com.ceiba.mobile.pruebadeingreso.objects

import org.json.JSONObject

class Company(company: JSONObject) {
    var name: String = company.getString("name")
    var catchPhrase: String = company.getString("catchPhrase")
    var bs: String = company.getString("bs")
}