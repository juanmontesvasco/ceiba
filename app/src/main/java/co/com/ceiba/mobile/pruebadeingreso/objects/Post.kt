package co.com.ceiba.mobile.pruebadeingreso.objects

import org.json.JSONObject

class Post(post: JSONObject) {
    var title: String = post.getString("title")
    var body: String = post.getString("body")
}