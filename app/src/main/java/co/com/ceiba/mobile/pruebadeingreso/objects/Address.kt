package co.com.ceiba.mobile.pruebadeingreso.objects

import org.json.JSONObject

class Address(address: JSONObject) {
    var street: String = address.getString("street")
    var suite: String = address.getString("suite")
    var city: String = address.getString("city")
    var zipcode: String = address.getString("zipcode")
    var geo: Geo = Geo(address.getJSONObject("geo"))
}